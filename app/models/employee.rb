class Employee < ActiveRecord::Base
  attr_accessible :company_id, :owner, :user_id

  belongs_to :company

  belongs_to( :employee,
  class_name: "User",
  foreign_key: :user_id,
  primary_key: :id)

end
