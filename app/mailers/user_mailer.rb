class UserMailer < ActionMailer::Base
  default from: "confirm@secure-inlet-2596.herokuapp.com"

  def welcome_email(user)
    @user = user
    @url  = 'http://secure-inlet-2596.herokuapp.com'
    mail(to: user.email, subject: 'Welcome to My Awesome Site')
  end

  def password_reset(user)
    @user = user
    mail(to: user.email, subject: "Password Reset")
  end

end
