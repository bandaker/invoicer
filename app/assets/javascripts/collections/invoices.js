INV.Collections.Invoices = Backbone.Collection.extend({
  initialize: function(hash){
    this.url = hash.url;
    this.tite = hash.title;
  },
  model: INV.Models.Invoice

});

INV.Collections.Invoices.prototype.total = function(){
  var total = 0;

  _.each( this.models, function(model){
    total += model.get("chargesTotal");
  });
  return total;
};