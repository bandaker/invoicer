INV.Views.ChargesView = Backbone.View.extend({

  template: JST['invoices/charges'],

  el: "#charges_modal",

  initialize: function() {
      var that = this;
      this.charges = INV.currentCharges;
      var renderCallback = that.render.bind(that);
      that.listenTo(this.charges, "add", renderCallback);
      that.listenTo(this.charges, "change", renderCallback);
      that.listenTo(this.charges, "remove", renderCallback);
      this.addHandlers();
  },

  addHandlers: function(){
    var that = this;

  },

  renderCharges: function() {
      var that = this;
      var total = that.currency(this.charges.total());

      this.$el.html(this.template({
        'charges': INV.currentCharges.models,
        "description": INV.currentInvoice.get("description"),
        "total": this.currency(INV.currentInvoice.get('chargesTotal'))
      }));

      this.modal(this.$el);
  }
});