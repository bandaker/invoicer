INV.Models.Company = Backbone.Model.extend({
  urlRoot: "/companies",
  isCurrent: function( user ) {
    return this == user.get('currentCompany') ? true : false;
  }
});
