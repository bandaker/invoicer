class CompaniesController < ApplicationController

  def new
    @company = Company.new
    respond_with(@company) do |format|
      format.json { render json: @company }
      format.html
    end
  end

  def edit
    @company = Company.find(params[:id])
  end

  def create
    @company = Company.create(params[:company])
    if @company.save
      Employee.create({
        company_id: @company.id,
        owner: true,
        user_id: current_user.id
      })
      redirect_to @company

    else
      flash.now[:errors] = @company.errors.full_messages
      render :new
    end
  end

  def index
    @companies = current_user.companies
    respond_with(@companies) do |format|
      format.json { render json: @companies }
      format.html
    end
  end


  def show
    @company = Company.find(params[:id])
  end

end
