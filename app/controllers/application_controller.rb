class ApplicationController < ActionController::Base
  include SessionsHelper
  include UsersHelper
  protect_from_forgery

  before_filter :logged_in?

  def logged_in?
    if current_user.nil?
      flash[:error] = 'You must sign in'
      redirect_to new_session_url
    end
  end


end