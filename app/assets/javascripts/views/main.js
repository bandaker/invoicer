INV.Views.MainView = Backbone.View.extend({

  template: JST['invoices/index'],

  el: '#invoices',

  invoices: null,

  initialize: function() {
    this.addHandlers();
  },

  renderCompanies: function() {
    this.companies.html('');

    var renderedCompany;

    _.each(INV.userCompanies, function( company){
      renderedCompany = JST['header/company']({
        currentCompany: company.isCurrent(INV.user),
        owner: INV.user.isOwner(company),
        company: company
      });
      this.companies.append(renderedCompany);
    });
  },

  addHandlers: function() {
    var that = this;
    $('#header').delegate('.mainitem','click',function(e){
      var item = $(this);
      var action = item.attr('data-action');
      that[action].apply(that, item);
    });
  },

  newInvoice: function() {
    INV.currentInvoice = new INV.Models.Invoice();
    INV.editInvoiceView.render();
  },

  // events: {
  //   'click .invoice': 'showCharges',
  //   'click choice': 'switchGroup'
  // },

  showCharges: function(e){

    console.log("showCharges firing");

    e.preventDefault();

    var invoiceId = $(e.currentTarget).attr('data-id');

    INV.currentInvoice = this.invoices.get(invoiceId);

    INV.currentCharges.url = "/invoices/" + invoiceId + "/charges";

    var that = this;
    INV.currentCharges.fetch({
      success: function(){
        INV.chargesView.renderCharges();
      },
      error: function(){
        console.log('not found');
      }
    });
  },

  switchGroup: function(e){
    e.preventDefault();
    console.log("FUCK")
    var that = this
    var desired_group = $(e.currentTarget).attr('data-id');
    if (desired_group === "received"){
      that.invoices = INV.invoiceGroups[0]
    } else {
      that.invoices = INV.invoiceGroups[1]
    }
    this.renderInvoices(this.invoices)

    console.log(desired_group)

  },

  render: function() {

      var that = this;

      _.each(this.collection.models,function( inv){
        _.each(inv.attributes, function( attr) {

        });
      });


      var total = that.currency(this.collection.total());

      // _.each( )
      console.log("WTFMAN")
      this.$el.html(this.template({
        'invoices': that.collection.models,
        "title": that.collection.title,
        "total": total
      }));

      return this;
  },

  renderInvoices: function( invoices ){
    this.stopListening();

    this.$el.html(JST['invoices/header']({
      title: invoices.title
    }));

    var that = this;

    var renderedInvoice;
    _.each( invoices.models, function( invoice ){
      renderedInvoice = JST['invoices/invoice']({
        invoice: invoice
      });

      that.$el.append(renderedInvoice);
    });
    var that = this;


  }

});