class Company < ActiveRecord::Base
  attr_accessible :addr1, :addr2, :country, :name, :state, :zip, :city


  has_many(:employee_relations,
  class_name: "Employee",
  foreign_key: :company_id,
  primary_key: :id)

  has_many(:owner_relations,
  class_name: "Employee",
  foreign_key: :company_id,
  primary_key: :id,
  conditions: {owner: true}
  )


  has_many(:employees, through: :employee_relations, source: :employee)

  has_many(:owners,
  through: :owner_relations,
  source: :employee)

  has_many(:invoices_sent,
  class_name: "Invoice",
  foreign_key: :sender_id,
  primary_key: :id)

  has_many(:invoices_received,
  class_name: "Invoice",
  foreign_key: :receiver_id,
  primary_key: :id)

  has_many(:sender_relations,
  class_name: "Relation",
  foreign_key: :sender_id,
  primary_key: :id)

  has_many( :debtors,
  through: :sender_relations,
  source: :receiver)

  has_many(:receiver_relations,
  class_name: "Relation",
  foreign_key: :receiver_id,
  primary_key: :id)

  has_many( :billers,
  through: :receiver_relations,
  source: :sender)

  # def as_json
#     {
#       company: self.id
#     }
#   end

end
