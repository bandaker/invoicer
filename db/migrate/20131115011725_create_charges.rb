class CreateCharges < ActiveRecord::Migration
  def change
    create_table :charges do |t|
      t.integer :invoice_id, null: false
      t.string :description, {default: ''}
      t.integer :amount, {default: 0}

      t.timestamps
    end
  end
end
