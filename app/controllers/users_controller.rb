class UsersController < ApplicationController
  skip_before_filter :logged_in?, :only => [:new, :create]

  layout "login", only: [:new]


  def index

  end

  def create
    @user = User.new(params[:user])

    if @user.save
      msg = UserMailer.welcome_email(@user)
      msg.deliver!
      login_user!(@user)
    else
      flash.now[:errors] = @user.errors.full_messages
      render :new
    end
  end

  def new
    @user = User.new
  end

  def show
    render json: current_user
  end

  def edit
    @user = User.find(params[:id])
  end

  def update_pass
    begin
      @user = User.find_by_password_reset_token(params[:password_reset_token])
      @user.password_reset_token = User.generate_token
      @user.password = params[:password]
      @user.save
      flash[:notice] = "Password Updated"
    rescue
      flash[:error] = "Fuck off"
    end
    redirect_to new_session_url
  end

  def update
    @user = current_user
    if @user
      @user.update_attributes(params[:user])
      flash[:notice] = "Updated"
      redirect_to root_url
    else
      redirect_to new_session_url
    end
  end

  def set_current_company
    # Add validation for actually being with the company
    current_user.current_company_id = params[:company_id]
    current_user.save!
    @user = current_user
    flash[:notice] = 'Current Company Updated'
    redirect_to edit_user_url(current_user.id)
  end

end
