class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.date :due_date, {default: (Date.today+60.days)}
      t.date :issue_date, {default: Date.today}
      t.text :description, {default: ""}
      t.integer :sender_id, null: false
      t.integer :receiver_id, null: false

      t.timestamps
    end
  end
end
