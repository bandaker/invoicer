class RelationsController < ApplicationController
  def new
    @companies = Company.all
    @relation = Relation.new
  end

  def create
    @relation = Relation.new(params[:relation])
    @relation.sender_id = current_company.id
    if @relation.save
      redirect_to new_invoice_url
    else
      flash.now[:errors] = @relation.errors
      render :new
    end
  end

  def index
    @relations = (current_company.billers + current_company.debtors).uniq
  end

  def destroy
  end

end
