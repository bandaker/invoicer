window.INV = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  initialize: function()
  {
    INV.invoicesHolder = $("#invoices");
    this.setupInvoices();
  },
  setupInvoices: function(){

    INV.user = new INV.Models.User({
      id: parseInt($('#user-id').html())
    });
    INV.user.fetch({
      success: function() {
        INV.mainView = new INV.Views.MainView();
        INV.invoiceGroups = {
          sent: new INV.Collections.Invoices({
            url: '/invoicessent',
            title: 'Invoices Sent'
          }),
          received: new INV.Collections.Invoices({
            url: '/invoicesreceived',
            title: 'Invoices Received'
          })
        };

        INV.invoiceGroups['received'].fetch({
          success: function(){
            INV.invoicesView = new INV.Views.InvoicesView( INV.invoiceGroups['received']);
          }
        });
      },
      error: function(){

      }
    });

    INV.editInvoiceView = new INV.Views.EditInvoiceView();

    INV.currentCharges =  new INV.Collections.Charges();

    INV.chargesView = new INV.Views.ChargesView();

  }
}

$(document).on('ready',function(){
  INV.initialize();
});