INV.Collections.Charges = Backbone.Collection.extend({
  model: INV.Models.Charge
});

INV.Collections.Charges.prototype.total = function(){
  var total = 0;

  _.each( this.models, function(charge){
    total += charge.get("amount");
  });
  return total;
};