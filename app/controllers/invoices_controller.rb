class InvoicesController < ApplicationController
  def index
    begin
      @invoices_sent = Invoice.find_all_by_sender_id(current_company.id)
      @invoices_received = Invoice.find_all_by_receiver_id(current_company.id)
    rescue
      @invoices_sent = []
      @invoices_received = []
      flash.now[:error] = "Your company is not set"
    end
  end


  def sent
    render json: Invoice.find_all_by_sender_id(current_company.id)
  end

  def received
    render json: Invoice.find_all_by_receiver_id(current_company.id)
  end

  def new
    if current_company
    	@invoice = Invoice.new
    	@company = current_company
      @relationships = (@company.billers + @company.debtors).uniq
      @users = User.all
    end
  end

  def create
    # Cover your ass here.
  	@invoice = Invoice.new(params[:invoice])
    @invoice.charges.new(params[:charges].values)
    @invoice.sender_id = current_company.id
  	@invoice.save!
  	redirect_to invoices_url
  end

  def show
  	@invoice = Invoice.find(params[:id])
  end

end
