_.extend(Backbone.View.prototype, {


  currency: function(integer) {
    var str = integer+""
    return '$' + str.substring(0,str.length-2) +"."+ str.substring(str.length-2);
  },

    overlay: $('overlay'),

    // why does using $('overlay') below work but this.overlay does not?
    //

    messenger: $('messenger'),

    failMessenger: $('failmessenger'),

    successMessenger: $('successmessenger'),

    modal: function(el) {
      if(el.hasClass('modal_active'))
      {
        el.removeClass('modal_active');
        this.hide($('overlay'), 200);
        this.hide(el, 200);
      } else {
        var that = this;
        $('overlay').one('click', function(){
          that.modal(el);
      });

      el.addClass('modal_active');
      console.log(this.overlay)
      console.log($('overlay'))
      this.unHide($('overlay'), 200);
      this.unHide(el, 200);
      }
    },

    modal2: function(el) {
      if(el.hasClass('modal_active'))
      {
        el.removeClass('modal_active');
        this.hide($('overlay2'), 200);
        this.hide(el, 200);
      } else {
        var that = this;
        $('overlay2').one('click', function(){
          that.modal2(el);
      });

      el.addClass('modal_active');
      console.log(this.overlay)
      console.log($('overlay2'))
      this.unHide($('overlay2'), 200);
      this.unHide(el, 200);
      }
    },

    hide: function(el, time) {
      el.addClass('hidden');
      setTimeout(
        function(){
          el.addClass('hide');
        }
        , time);
    },
    unHide: function(el, time) {
      el.removeClass('hide');
      setTimeout(
        function(){
          el.removeClass('hidden');
        }
        , time);
    },

    empty: function(el,time){
      setTimeout(function(){
        el.html('')
      }, time);
    },

    hideShow: function(el1, el2, time){
      this.hide(el1,time);
      var that = this;
      setTimeout(
        function(){
          that.unHide(el2,time);
        }
        , time);
    },

    messageSuccess: function(m, time) {
      var s = this.successMessenger;
      s.html(m);
      var that = this;
      this.unHide(s,200);
      setTimeout(
        function(){
          that.hide(s,200);
        }, time);
    },
    messageFail: function(m,time) {
      var f = this.failMessenger;
      f.html(m);
      var that = this;
      this.unHide(f,200);
      setTimeout(
        function(){
          that.hide(f,200);
        }, time);
    },
    messageNotify: function(m,time) {
      var mes = this.messenger;
      mes.html(m);
      var that = this;
      this.unHide(mes,200);
      setTimeout(
        function(){
          that.hide(mes,200);
        }, time);
    }

});
