class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :addr1
      t.string :addr2
      t.string :state
      t.string :country
      t.string :zip

      t.timestamps
    end
  end
end
