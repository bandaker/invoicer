class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :fname
      t.string :lname
      t.string :addr1
      t.string :addr2
      t.string :state
      t.string :country
      t.string :zip
      t.string :company
      t.string :password_digest

      t.timestamps
    end
  end
end
