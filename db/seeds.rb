# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
companies_count = 0



u = User.new("")
100.times do |i|
  u = User.new(
  fname: Faker::Name.first_name,
  lname: Faker::Name.last_name,
  current_company_id: nil,
  password: "password",
  email: "fake#{i}@fake.com",
  )

  u.save

  Random.rand(2) == 0 ? 0 : Random.rand(3).times do
    c = Company.new(
    addr1: Faker::Address.street_address,
    city: Faker::Address.city,
    state: Faker::Address.state_abbr,
    country: "USA",
    zip: Faker::Address.zip_code,
    name: Faker::Company.name,
    )
    c.save
    companies_count += 1

    Employee.create({
      company_id: c.id,
      owner: true,
      user_id: u.id
    })
    u.current_company_id = Random.rand(3) == 0 ? nil : c.id

  end
  u.save
  if u.companies.length == 0 && companies_count != 0
    Employee.create({
      company_id: Random.rand(companies_count) + 1,
      owner: false,
      user_id: u.id
    })
  end
end




def get_inv_desc
  choice = Random.rand(4)
  case choice

  when 0
   return Faker::Lorem.sentence
  when 1
    return Faker::Lorem.words.join(' ')
  when 2
    return Faker::Company.bs + ' and ' + Faker::Company.bs
  when 3
    return Faker::Lorem.paragraph
  end

end

def get_charge_desc
  choice = Random.rand(2)
  case choice

  when 0
   return Faker::Lorem.sentence
  when 1
    return Faker::Lorem.words.join(' ')
  end

end

200.times do |i|
  pool = (0..companies_count).to_a
  sid = pool.shuffle.pop
  rid = pool.shuffle.pop
  issuedate = Date.today - (sid + rid) % 60
  inv = Invoice.new(
    sender_id: sid,
    receiver_id: rid,
    due_date: (issuedate+55),
    issue_date: issuedate,
    description: get_inv_desc
  )
  inv.save

  if inv.sender != nil && !inv.sender.debtors.include?(inv.receiver_id)
    r = Relation.new()
    r.receiver_id = inv.receiver_id
    r.sender_id = inv.sender_id
    r.save
  end

  Random.rand(12).times do |j|
    charge = Charge.new(
      invoice_id: inv.id,
      description: get_charge_desc,
      amount: Random.rand(2000)
    )
    charge.save
  end
end

