class Relation < ActiveRecord::Base
  attr_accessible :receiver_id, :sender_id

  belongs_to( :sender,
  class_name: 'Company',
  foreign_key: :sender_id,
  primary_key: :id
  )

  belongs_to( :receiver,
  class_name: 'Company',
  foreign_key: :receiver_id,
  primary_key: :id
  )
end
