INV.Models.User = Backbone.Model.extend({
  urlRoot: "/users",
  isOwner: function( company ) {
    return _.include(this.ownedCompanies, company );
  },
  parse: function( json){
    console.log(json);

    this.set( {clients: new INV.Collections.Companies( json.companies )});

    this.set(json.user);
  }
});
