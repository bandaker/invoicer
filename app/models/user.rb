class User < ActiveRecord::Base
  attr_accessible :addr1, :addr2, :country, :current_company_id, :fname, :lname, :state,
  :zip, :password_digest, :password, :email, :session_token, :city

  has_many(:company_positions,
  class_name: "Employee",
  foreign_key: :user_id,
  primary_key: :id)

  has_many( :companies,
  through: :company_positions,
  source: :company)




  def self.find_by_credentials(email,password)
    user = User.find_by_email(email)
    msg = 'User Not Found'
    raise StandardError, msg unless user
    msg =  'Invalid Login'
    raise StandardError, msg unless user.is_password?(password)

    user
  end

  def self.generate_token
    SecureRandom::urlsafe_base64(16)
  end

  def has_session_token
    if self.session_token.nil?
      reset_session_token
    end
  end

  def reset_session_token
    self.session_token = self.class.generate_token
    self.save!
  end

  def send_password_reset_token
    @reset_password_token = User.generate_token
    @password_reset_sent_at = Time.now
    self.save!

    UserMailer.password_reset(self).deliver!
  end
  def password=(password)
    self.password_digest = BCrypt::Password.create(password)
  end

  def is_password?(password)
    BCrypt::Password.new(self.password_digest).is_password?(password)
    #self.password_digest == BCrypt::Password.new(password)
  end

  def as_json(user)
    hash = self.attributes
    hash.except!( 'password_digest', 'password',  'session_token')
    hash[:currentCompany] = Company.find(self.current_company_id)

    {
      user: hash,
      companies: self.companies
    }
  end

end
