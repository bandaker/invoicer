class Charge < ActiveRecord::Base
  attr_accessible :amount, :date, :description, :invoice_id

  belongs_to( :invoice,
  class_name: "Invoice",
  foreign_key: :invoice_id,
  primary_key: :id)

  def sender
  	invoice.sender
  end

  def receiver
  	invoice.receiver
  end

end

