# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20131128231600) do

  create_table "charges", :force => true do |t|
    t.integer  "invoice_id",                  :null => false
    t.string   "description", :default => ""
    t.integer  "amount",      :default => 0
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "companies", :force => true do |t|
    t.string   "name"
    t.string   "addr1"
    t.string   "addr2"
    t.string   "state"
    t.string   "country"
    t.string   "zip"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "city"
  end

  create_table "employees", :force => true do |t|
    t.integer  "user_id"
    t.integer  "company_id"
    t.boolean  "owner"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "invoices", :force => true do |t|
    t.date     "due_date",    :default => '2014-02-26'
    t.date     "issue_date",  :default => '2013-12-28'
    t.text     "description", :default => ""
    t.integer  "sender_id",                             :null => false
    t.integer  "receiver_id",                           :null => false
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
  end

  create_table "relations", :force => true do |t|
    t.integer  "sender_id"
    t.integer  "receiver_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "fname"
    t.string   "lname"
    t.string   "addr1"
    t.string   "addr2"
    t.string   "state"
    t.string   "country"
    t.string   "zip"
    t.string   "password_digest"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.string   "email"
    t.string   "session_token"
    t.string   "password_reset_token"
    t.string   "city"
    t.integer  "current_company_id"
  end

end
