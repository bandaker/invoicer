module UsersHelper
  def current_company
    return nil if current_user.current_company_id.nil?
    @current_company ||= Company.find(current_user.current_company_id)
    #
  end

end
