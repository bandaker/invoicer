class SessionsController < ApplicationController
  skip_before_filter :logged_in?, :only => [:new, :create]


  layout "login", only: [:new,:create]
  def create
    # fail
    begin
      @user = User.find_by_credentials(
        params[:user][:email],
        params[:user][:password]
      )
      # fail
      login_user!(@user)

      @data = {
        user: @user,
        companies: @user.companies
      }
    # DAMNIT DAN! fancy rescue method broke login.

  rescue StandardError => e
     flash.now[:error] = e.message
     render :new
    #   redirect_to root_url
  end
  end

  def destroy
    current_user.reset_session_token
    redirect_to root_url
  end


end

