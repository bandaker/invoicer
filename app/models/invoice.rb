class Invoice < ActiveRecord::Base
  attr_accessible :description, :due_date, :issue_date, :issuer_id, :sender_id, :receiver_id

  has_many( :charges,
  class_name: "Charge",
  foreign_key: :invoice_id,
  primary_key: :id)

  belongs_to(:sender,
  class_name: "Company",
  foreign_key: :sender_id,
  primary_key: :id)

  belongs_to(:receiver,
  class_name: "Company",
  foreign_key: :receiver_id,
  primary_key: :id)

  def total
    self.charges.sum{|charge| charge.amount}
  end


end
