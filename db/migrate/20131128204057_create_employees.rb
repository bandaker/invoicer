class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.integer :user_id
      t.integer :company_id
      t.boolean :owner

      t.timestamps
    end
  end
end
