INV.Models.Invoice = Backbone.Model.extend({
  urlRoot: "/invoices",

  today: function(){
    if ( this.isNew() ) {
      return new Date();
    } else {
      return this.get('issue_date');
    }
  },

  due: function() {
    if ( this.isNew() ){
       var due = new Date();
       due.setMonth((new Date().getMonth()+2)%12);
       return due;
    } else {
      return this.get('due_date');
    }
  }
});
