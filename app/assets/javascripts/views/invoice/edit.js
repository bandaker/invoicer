INV.Views.EditInvoiceView = Backbone.View.extend({
  initialize: function(){
    this.addHandlers();
  },


  el: '#form_modal',

  addHandlers: function(){
    var that = this;
    $('#form_modal').delegate('.editAction','click', function(){
      var item = $(this);
      var action = item.attr('data-action');
      that[action].apply(that);
    });

    $('#form_modal').delegate('#new_company_form form','submit',function(e){

      e.preventDefault();
      that.newCompany.save($(this).serializeJSON(),{
        success: function(){
          that.modal2($('#new_company_form'));
          that.renderClient(that.newCompany);
        },
        error: function(resp){
          console.log('error',resp)
        }
      })
    })
  },

  addClient: function() {
    this.newCompany = new INV.Models.Company()
    var that = this;
    $('#new_company_form').html( JST['companies/new']({
      company: that.newCompany
    }));

    this.modal2($('#new_company_form'));
  },

  renderClient: function(client){
    var renderedClient = JST['charges/client']({
      company: client
    })
    $('#potentialclients').append(renderedClient);
  },

  render: function() {
    this.$el.html(JST['invoices/form']({
      invoice: INV.currentInvoice,
      today: INV.currentInvoice.today(),
      due: INV.currentInvoice.due(),
      current_company: INV.user.get('currentCompany')
    }));

    var that = this;

    _.each( INV.user.get('clients').models, function( client ){
      that.renderClient(client);
    });

    $('#charges').html('');

    if ( !INV.currentInvoice.isNew() ){
      this.renderCharges();
    }

    this.modal(this.$el);

  },

  renderCharges:function() {

    var renderedCharge;

    _.each(INV.currentInvoice.charges.models, function( charge ){
      renderedCharge = JST['charges/charge']({
        charge: charge
      });

      $('#charges').append(renderedCharge);
    });

  }

});