INV.Views.InvoicesView = Backbone.View.extend({

  template: JST['invoices/index'],

  el: '#invoices',

  invoices: null,

  initialize: function(invoices) {

    this.renderInvoices(invoices)

    this.invoices = invoices;

    var that = this;

    var renderCallback = that.renderInvoices.bind(that);

    that.listenTo(that.invoices, "add", renderCallback);
    that.listenTo(that.invoices, "change", renderCallback);
    that.listenTo(that.invoices, "remove", renderCallback);
  },

  events: {
    'click .invoice': 'showCharges',
    'click choice': 'switchGroup'
  },

  showCharges: function(e){

    console.log("showCharges firing");

    e.preventDefault();

    var invoiceId = $(e.currentTarget).attr('data-id');

    INV.currentInvoice = this.invoices.get(invoiceId);

    INV.currentCharges.url = "/invoices/" + invoiceId + "/charges";

    var that = this;

    INV.currentCharges.fetch({
      success: function(){
        INV.chargesView.renderCharges();
      },
      error: function(){
        console.log('not found');
      }
    });
  },

  switchGroup: function(e){
    e.preventDefault();
    var that = this;
    var desired_group = $(e.currentTarget).attr('data-id');

    that.invoices = INV.invoiceGroups[desired_group]

    that.invoices.fetch({
      success: function(){
          that.renderInvoices(that.invoices);
      }
    });


    console.log(desired_group);

  },

  newInvoice: function(e){
    e.preventDefault();
    console.log("NewInvoice Firing");
  },

  render: function() {

      var that = this;

      _.each(this.collection.models,function( inv){
        _.each(inv.attributes, function( attr) {

        });
      });


      var total = that.currency(this.collection.total());

      // _.each( )
      console.log("WTFMAN")
      this.$el.html(this.template({
        'invoices': that.collection.models,
        "title": that.collection.title,
        "total": total
      }));

      return this;
  },

  renderInvoices: function( invoices ){
    this.stopListening();

    this.$el.html(JST['invoices/header']({
      title: invoices.title,
      total: invoices.total()
    }));

    var that = this;

    var renderedInvoice;
    _.each( invoices.models, function( invoice ){
      renderedInvoice = JST['invoices/invoice']({
        invoice: invoice
      });

      that.$el.append(renderedInvoice);
    });
    var that = this;


  }

});