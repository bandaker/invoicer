class ChangeCompanyIdToCurrentCompanyId < ActiveRecord::Migration
  def change
    rename_column :users, :company_id, :current_company_id
  end
end
